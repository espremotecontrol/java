package it.lundstedt.erik;
import it.lundstedt.erik.menu.Menu;

import java.io.IOException;
public class Main
{
static boolean areYouSure()
{
	String[] header={"are you sure","you want to quit?"};
	String[] menuItems={"Y/N"};
	Menu.drawMenu(header,menuItems);
	
	switch (Menu.getChoiceStr())
	{
		case "y":
		case "Y":
		case "yes":
		case "Yes":
			return true;
		case "n":
		case "N":
		case "no":
		case "No":
			return false;
		default:
			throw new IllegalStateException("Unexpected value: " + Menu.getChoiceStr());
	}
}

public static void main(String[] args) throws IOException, InterruptedException {
	Menu.licence("openESPControl", "0.1");
	String[] dataString = new String[2];
	dataString[0] = "";
	dataString[1] = "";
	boolean doDebug = true;
	boolean quit = false;
	String[] header = {};
	String[] menuItems = {"0:turn on", "1:turn off", "2:toggle", "99:quit","what do you want to do?"};
	restAPI rest = new restAPI();
	Menu mainMenu = new Menu();
	/*mainMenu.setHeader(header);
	mainMenu.setMenuItems(menuItems);
	//String dataString;
	Menu.drawItems(dataString);
	mainMenu.drawMenu();*/
	
	while (quit == false)
	{
		mainMenu.setHeader(header);
		mainMenu.setMenuItems(menuItems);
		//String dataString;
		mainMenu.drawMenu();
		Menu.drawItems(dataString);
		dataString[0] = "";
		dataString[1] = "";
	
		switch (Menu.getChoiceStr()) {
			case "1"://on
				dataString = rest.request("on");
				break;
			case "2"://off
				dataString = rest.request("off");
				break;
			case "3"://toggle
				dataString = rest.request("toggle");
				break;
			case "99":
				try {
					
					if (areYouSure()) {
						quit=true;
					} else if (!areYouSure()) {
					
					}
				} catch (IllegalStateException err) {
					Menu.error(err.toString());
				}
			case "99 -y":
				System.out.println("godbye....");
				quit=true;
				break;
		}
	}
}
}


