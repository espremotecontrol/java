package it.lundstedt.erik;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class restAPI
{
	private static String ip="http://192.168.0.116";

	public static void setIp(String ip) {
		restAPI.ip = ip;
	}

	private static HttpClient client = HttpClient.newHttpClient();
	HttpRequest toogle = HttpRequest.newBuilder()
			.uri(URI.create(this.ip+"/led/toggle"))
			.build();
	HttpRequest on = HttpRequest.newBuilder()
			.uri(URI.create(this.ip+"/led/on"))
			.build();
	HttpRequest off = HttpRequest.newBuilder()
			.uri(URI.create(this.ip+"/led/off"))
			.build();
	HttpRequest getStatus = HttpRequest.newBuilder()
			.uri(URI.create(this.ip+"/led"))
			.build();



	private String getResponse(HttpRequest http) throws IOException, InterruptedException {
		HttpResponse<String> response = client.send(http,
		HttpResponse.BodyHandlers.ofString());
		//System.out.println(response.body());
		System.out.flush();
		return response.body();
	}


	public String[] request(String option) throws IOException, InterruptedException {
		String[] dataString=new String[2];
		       dataString[0]="";
		switch (option)
		{
			case "on":
				dataString[0]=getResponse(on);
				break;
			case "off":
				dataString[0]=getResponse(off);
				break;
			case "toogle":
				dataString[0]=getResponse(toogle);
				break;
		}
		dataString[1]="";
		dataString[1]=getResponse(getStatus);
		return dataString;
	}
}





